0. Install Grafana -
```
wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_4.6.3_amd64.deb
sudo apt-get install -y adduser libfontconfig
sudo dpkg -i grafana_4.6.3_amd64.deb
```
1. Start Grafana - sudo service grafana-server start
2. Logs - /var/log/grafana
3. Config - /etc/grafana/grafana.ini